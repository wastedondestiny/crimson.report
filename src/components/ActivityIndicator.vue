<script setup lang="ts">
import { computed } from 'vue'
import { type Activity, ActivityId, ActivityType } from '../enums/activities'
import { micromark } from 'micromark'
import GenericExpansionIcon from './GenericExpansionIcon.vue'
import { activityDetail } from '../releaseDetail'
import { type Account, TheTooltip } from '@wastedondestiny/destiny-library'

const props = withDefaults(defineProps<{
  account: Account,
  accounts: Account[],
  activity: Activity,
  rotations: Record<number, string>,
  strikeIfNotInRotation?: boolean,
  hideIfOutOfRotation?: boolean,
  neverLatest?: boolean,
  noInteract?: boolean
}>(), {
  strikeIfNotInRotation: false,
  hideIfOutOfRotation: false,
  neverLatest: false,
  noInteract: false
})

const whoHas = computed(() =>
  props.accounts.filter(account =>
    props.activity.ownership.some(x =>
      ((account.versionsOwned ?? 0) & x) === x ||
      account.seasonsOwned?.includes(x) ||
      account.dungeonsOwned?.includes(x)
    )
  ).length
)
const everyoneHas = computed(() =>
  !props.activity.ownership.length ||
  whoHas.value === props.accounts.length
)
const someHave = computed(() =>
  !everyoneHas.value &&
  whoHas.value > 0 &&
  whoHas.value <= props.accounts.length
)
const iHave = computed(() =>
  !props.activity.ownership.length || 
  props.activity.ownership.some(x =>
    ((props.account.versionsOwned ?? 0) & x) === x ||
    props.account.seasonsOwned?.includes(x) ||
    props.account.dungeonsOwned?.includes(x)
  )
)
const enoughHave = computed(() =>
  props.activity.maxPlayers &&
  someHave.value &&
  whoHas.value >= props.activity.maxPlayers &&
  iHave.value
)
const noOneHas = computed(() =>
  !iHave.value &&
  !someHave.value
)
const activityIcon = computed(() => props.activity.type === ActivityType.Dungeon
  ? props.activity.releasedWith
  : (props.activity.ownership?.[0] ?? props.activity.releasedWith))
const rotator = computed(() => props.rotations[props.activity.id])
const isInRotation = computed(() => rotator.value?.startsWith('Active Weekly'))
const isOutOfRotation = computed(() => !rotator.value && props.activity.type === ActivityType.Nightfall)
const isLatest = computed(() => {
  if (props.neverLatest) return false
  const isRotatingActivity = [ActivityType.Dungeon, ActivityType.ExoticMission, ActivityType.Raid].includes(props.activity.type)
  const otherActivities = activityDetail.filter(x => x.type === props.activity.type).sort((a, b) => b.date.getTime() - a.date.getTime())
  return isRotatingActivity && otherActivities[0].id === props.activity.id
})
const description = computed(() => {
  if (props.strikeIfNotInRotation && !isInRotation.value && !isLatest.value) {
    return `
> **Note**: This activity is not available at this moment. It will come back around another week!
    
${props.activity.description}`
  }

  return props.activity.description
})
</script>

<template>
  <div class="w-full py-0.5 px-2 rounded-2xl flex justify-between items-center border-2" :class="{
    'bg-emerald-300 border-emerald-300': everyoneHas || enoughHave,
    'bg-yellow-200 border-yellow-200': iHave && someHave && !enoughHave,
    'bg-zinc-100 border-zinc-100 text-black/50': noOneHas || !iHave,
    'hidden': hideIfOutOfRotation && isOutOfRotation,
    'border-dashed bg-opacity-50': strikeIfNotInRotation && !isInRotation && !isLatest
  }">
    <span class="flex items-center pr-1 text-black" :class="{ 'opacity-50': strikeIfNotInRotation && !isInRotation && !isLatest }">
      <TheTooltip :no-interact="noInteract" other-classes="flex items-center gap-1 text-sm">
        <GenericExpansionIcon :expansion="activityIcon" other-classes="h-4 w-4" />
        <span class="text-3xl leading-3 inline-block w-3 text-center" v-if="isInRotation || isLatest">&bullet;</span>
        <slot />
        <small class="bg-zinc-100/50 px-1.5 py-0.5 inline-block rounded-xl text-xs h-4 leading-none" v-if="activity.maxPlayers && !(strikeIfNotInRotation && !isInRotation && !isLatest)">{{ !activity.ownership.length ? accounts.length : whoHas }}/{{ activity.maxPlayers }}</small>
        <template #icon>
          <img v-if="activity.type === ActivityType.Raid" src="../assets/raid.svg" alt="Raid" />
          <img v-if="activity.type === ActivityType.Dungeon" src="../assets/dungeon.png" alt="Dungeon" />
          <img v-if="activity.type === ActivityType.Nightfall" src="../assets/strike.svg" alt="Nightfall strike" />
          <img v-if="activity.type === ActivityType.CoreActivity" src="../assets/destiny.svg" alt="Core activity" />
          <img v-if="activity.type === ActivityType.ExoticMission" src="../assets/engram.svg" alt="Exotic mission" />
          <img v-if="activity.type === ActivityType.SeasonalActivity" src="../assets/quest.svg" alt="Seasonal activity" />
          <img v-if="activity.type === ActivityType.ExpansionActivity" src="../assets/adventure.svg" alt="Expansion activity" />
        </template>
        <template #content>
          <span class="block"><slot /></span>
          <span class="text-xs whitespace-nowrap">Released {{ activity.date.toLocaleDateString('en-US', { dateStyle: 'long' }) }}</span>
          <span class="markdown" v-html="micromark(description)" />
        </template>
      </TheTooltip>
    </span>
    <template v-if="!activity.ownership.length">
      <TheTooltip :no-interact="noInteract" other-classes="flex-shrink-0">
        <img src="../assets/free.svg" class="h-4 w-4 opacity-75" alt="This activity is free to play for all players" />
        <template #icon>
          <img src="../assets/free.svg" alt="This activity is free to play for all players" />
        </template>
        <template #content>This activity is free to play for all players</template>
      </TheTooltip>
    </template>
    <template v-if="activity.ownership.length && everyoneHas">
      <TheTooltip :no-interact="noInteract" other-classes="flex-shrink-0">
        <img src="../assets/check.svg" class="h-4 w-4 opacity-75" alt="Everyone has access to this activity" />
        <template #icon>
          <img src="../assets/check.svg" alt="Everyone has access to this activity" />
        </template>
        <template #content>Everyone has access to this activity</template>
      </TheTooltip>
    </template>
    <template v-if="activity.ownership.length && enoughHave">
      <TheTooltip :no-interact="noInteract" other-classes="flex-shrink-0">
        <img src="../assets/check.svg" class="h-4 w-4 opacity-75" alt="Enough people have access to this activity" />
        <template #icon>
          <img src="../assets/check.svg" alt="Enough people have access to this activity" />
        </template>
        <template #content>Enough people have access to this activity</template>
      </TheTooltip>
    </template>
    <template v-if="activity.ownership.length && iHave && someHave && !enoughHave">
      <TheTooltip warning :no-interact="noInteract" other-classes="flex-shrink-0">
        <img src="../assets/warning.svg" class="h-4 w-4 opacity-75" alt="Not enough people have access to this activity" />
        <template #icon>
          <img src="../assets/warning.svg" alt="Not enough people have access to this activity" />
        </template>
        <template #content>Not enough people have access to this activity</template>
      </TheTooltip>
    </template>
  </div>
</template>
