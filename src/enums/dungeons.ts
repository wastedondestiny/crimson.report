import { ActivityId } from './activities'

export enum WitchQueenDungeonHashes {
  Duality = ActivityId.Duality,
  SpireOfTheWatcher = ActivityId.SpireOfTheWatcher
}

export enum LightfallDungeonHashes {
  GhostsOfTheDeep = ActivityId.GhostsOfTheDeep,
  WarlordsRuin = ActivityId.WarlordsRuin
}

export enum FinalShapeDungeonHashes {
  VespersHost = ActivityId.VespersHost,
  SunderedDoctrine = ActivityId.SunderedDoctrine
}
