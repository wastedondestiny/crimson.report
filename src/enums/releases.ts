import { Expansions } from './expansions'
import { BeyondLightSeasonHashes, FinalShapeEpisodeHashes, LightfallSeasonHashes, ShadowkeepSeasonHashes, WitchQueenSeasonHashes } from './seasons'
import { FinalShapeDungeonHashes, LightfallDungeonHashes, WitchQueenDungeonHashes } from './dungeons'

export type SeasonId = ShadowkeepSeasonHashes | BeyondLightSeasonHashes | WitchQueenSeasonHashes | LightfallSeasonHashes
export type DungeonId = WitchQueenDungeonHashes | LightfallDungeonHashes | FinalShapeDungeonHashes
export type ReleaseId = Expansions | SeasonId | DungeonId | FinalShapeEpisodeHashes
export type Release = {
  name: string,
  id: ReleaseId,
  date: Date,
  bought: string,
  active: boolean,
  hidden: boolean,
  shortDescription: string,
  longDescription: (string|number[])[],
  deluxeDescription?: string
}
