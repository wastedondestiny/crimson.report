import fetch from 'node-fetch'
import { existsSync } from 'node:fs'
import { writeFile, rm, mkdir } from 'node:fs/promises'
import { activityDetail, releaseDetail } from './releaseDetail'
import { ActivityType } from './enums/activities'

type ManifestResponse = {
  Response: {
    version: string,
    jsonWorldComponentContentPaths: {
      en: {
        DestinyInventoryItemLiteDefinition: string,
        DestinyPresentationNodeDefinition: string,
        DestinyRecordDefinition: string
      }
    }
  }
}
type ItemDefinition = Record<number, {
  displayProperties: {
    name: string,
    icon: string
  },
  itemTypeAndTierDisplayName: string,
  iconWatermark: string
}>
type PresentationNodeDefinition = Record<number, {
  children: {
    records: {
      recordHash: number
    }[]
  }
}>
type RecordDefinition = Record<number, {
  displayProperties: {
    name: string
  },
  forTitleGilding: boolean
}>

const noop = () => {
  // No operation
}

const getManifest = async () => {
  const manifestUrl = 'https://www.bungie.net/Platform/Destiny2/Manifest/'
  const manifestResult = await fetch(manifestUrl)
  return await manifestResult.json() as ManifestResponse
}

const createFolders = async () => {
  if (existsSync('./src/generated')) await rm('./src/generated', { recursive: true })
  await mkdir('./src/generated')

  if (existsSync('./public/generated')) await rm('./public/generated', { recursive: true })
  await mkdir('./public/generated')
}

const nightfallSanitizer = (activityName: string) => activityName
  .replace('Grandmaster:', '')
  .replace('Battlegrounds', 'Battleground')
  .trim()

const generateNightfalls = async (manifestData: ManifestResponse) => {
  console.log(`Generating nightfalls from ${manifestData.Response.version}`)

  const presentationNodeDefinitionUrl = `https://www.bungie.net${manifestData.Response.jsonWorldComponentContentPaths.en.DestinyPresentationNodeDefinition}`
  const presentationNodeDefinitionResult = await fetch(presentationNodeDefinitionUrl)
  const presentationNodeDefinitionData: PresentationNodeDefinition = await presentationNodeDefinitionResult.json() as never

  const recordDefinitionUrl = `https://www.bungie.net${manifestData.Response.jsonWorldComponentContentPaths.en.DestinyRecordDefinition}`
  const recordDefinitionResult = await fetch(recordDefinitionUrl)
  const recordDefinitionData: RecordDefinition = await recordDefinitionResult.json() as never

  const title = presentationNodeDefinitionData[3776992251]

  const records = title.children.records
    .map(x => recordDefinitionData[x.recordHash])
    .filter(x => x.forTitleGilding)
    .map(x => ({
      name: nightfallSanitizer(x.displayProperties.name),
      activityHash: activityDetail.filter(y => y.type === ActivityType.Nightfall && y.active).find(y => y.name === nightfallSanitizer(x.displayProperties.name))?.id
    }))
  
  await writeFile('./src/generated/nightfallDefinitions.json', JSON.stringify(records))

  console.log(`Generated ${records.length} nightfalls!`)
}

const generateItems = async (manifestData: ManifestResponse) => {
  console.log(`Generating items from ${manifestData.Response.version}`)

  const itemDefinitionUrl = `https://www.bungie.net${manifestData.Response.jsonWorldComponentContentPaths.en.DestinyInventoryItemLiteDefinition}`
  const itemDefinitionResult = await fetch(itemDefinitionUrl)
  const itemDefinitionData: ItemDefinition = await itemDefinitionResult.json() as never

  const items = Array.from(
    Array.from(releaseDetail.values())
      .flatMap(x => x.longDescription.filter(x => typeof x !== 'string').flatMap(x => x as number[]) as number[])
      .reduce((set, x) => set.add(x), new Set<number>())
  )
  const imagePaths = [...new Set(items.flatMap(x => [itemDefinitionData[x]?.displayProperties?.icon, itemDefinitionData[x]?.iconWatermark]))]
  const itemDefinitions = items.map(x => ({
    id: x,
    name: itemDefinitionData[x]?.displayProperties?.name,
    icon: itemDefinitionData[x]?.displayProperties?.icon?.split(/(\\|\/)/g).pop(),
    type: itemDefinitionData[x]?.itemTypeAndTierDisplayName,
    watermark: itemDefinitionData[x]?.iconWatermark?.split(/(\\|\/)/g).pop()
  }))
  
  await writeFile('./src/generated/itemDefinitions.json', JSON.stringify(itemDefinitions))
  await Promise.all(imagePaths.map(x => fetch(`https://bungie.net${x}`).then(y => y.buffer()).then(y => writeFile(`./public/generated/${x.split(/(\\|\/)/g).pop()}`, y)).catch(noop)))

  console.log(`Generated ${itemDefinitions.length} items!`)
}

const generate = async () => {
  const [manifestData] = await Promise.all([
    getManifest(),
    createFolders()
  ])
  await Promise.all([
    generateItems(manifestData),
    generateNightfalls(manifestData)
  ])
}

generate()
