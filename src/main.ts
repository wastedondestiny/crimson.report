import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './routes'
import Tracker from '@openreplay/tracker'
import * as Sentry from '@sentry/vue'

try {
  const tracker = new Tracker({
    projectKey: 'Qlx2V4F39lcjazJ0i5Ll',
    ingestPoint: 'https://mon.binar.ca/ingest',
  })
  tracker?.start()
} catch (_) {
  // noop
}

window.googleAdsClient = import.meta.env.VITE_GOOGLE_ADS_CLIENT
window.googleAdsSlot = import.meta.env.VITE_GOOGLE_ADS_SLOT

const app = createApp(App)

Sentry.init({
  app,
  dsn: 'https://9fb47b1f4fd8477fa94d181aea1a6116@log.binar.ca/5',
  integrations: [
    Sentry.browserTracingIntegration({ router }),
    Sentry.replayIntegration()
  ],
  tracesSampleRate: 1.0,
  replaysSessionSampleRate: 0.1,
  replaysOnErrorSampleRate: 1.0,
})

app.use(router)
app.mount('#app')

console.log('%ccrimson.report loaded!', 'color:red;-webkit-text-stroke:1px black;font-size:36px;')
setTimeout(() => console.log('You wouldn\'t paste code a stranger online gave you into this console, would you?'), 1000)
