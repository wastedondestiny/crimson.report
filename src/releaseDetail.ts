import { Release } from './enums/releases'
import { Activity, ActivityId, ActivityType } from './enums/activities'
import { Expansions } from './enums/expansions'
import { BeyondLightSeasonHashes, FinalShapeEpisodeHashes, LightfallSeasonHashes, ShadowkeepSeasonHashes, WitchQueenSeasonHashes } from './enums/seasons'
import { FinalShapeDungeonHashes, LightfallDungeonHashes, WitchQueenDungeonHashes } from './enums/dungeons'

export const activityDetail: Activity[] = [
  {
    name: 'Vanguard Ops',
    id: ActivityId.Strike,
    type: ActivityType.CoreActivity,
    active: true,
    date: new Date('2017-09-06 09:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Destiny2,
    ownership: [],
    description: `Vanguard Ops are on a random rotation and are always free to play for all players, even if you don't own the expansion related to a specific strike. You may not manually select a strike for which you do not own the expansion, however.`
  },
  {
    name: 'The Arms Dealer',
    id: ActivityId.TheArmsDealer,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2017-09-06 09:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Destiny2,
    ownership: [],
    description: `The Arms Dealer is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Lake of Shadows',
    id: ActivityId.LakeOfShadows,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2017-09-06 09:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Destiny2,
    ownership: [],
    description: `Lake of Shadows is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Savathûn\'s Song',
    id: ActivityId.SavathunsSong,
    type: ActivityType.Nightfall,
    active: false,
    date: new Date('2017-09-06 09:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Destiny2,
    ownership: [Expansions.Destiny2],
    description: `Savathûn's Song is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
    Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Exodus Crash',
    id: ActivityId.ExodusCrash,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2017-09-06 09:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Destiny2,
    ownership: [],
    description: `Exodus Crash is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Inverted Spire',
    id: ActivityId.InvertedSpire,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2017-09-06 09:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Destiny2,
    ownership: [],
    description: `Inverted Spire is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'The Pyramidion',
    id: ActivityId.ThePyramidion,
    type: ActivityType.Nightfall,
    active: false,
    date: new Date('2017-09-06 09:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Destiny2,
    ownership: [Expansions.Destiny2],
    description: `The Pyramidion is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Crucible',
    id: ActivityId.Crucible,
    type: ActivityType.CoreActivity,
    active: true,
    date: new Date('2017-09-06 09:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.Destiny2,
    ownership: [],
    description: `Crucible is the PvP of Destiny 2. It includes every game mode, every map and every event except Trials of Osiris.
    
Crucible is linked to the title Glorious.`
  },
  {
    name: 'Leviathan',
    id: ActivityId.Leviathan,
    type: ActivityType.Raid,
    active: false,
    date: new Date('2017-09-13 09:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.Destiny2,
    ownership: [Expansions.Destiny2],
    description: ``
  },
  {
    name: 'Trials of the Nine',
    id: ActivityId.TrialsOfTheNine,
    type: ActivityType.CoreActivity,
    active: false,
    date: new Date('2017-09-15 09:00:00 UTC'),
    maxPlayers: 4,
    releasedWith: Expansions.Destiny2,
    ownership: [Expansions.Destiny2],
    description: ``
  },
  {
    name: 'Tree of Probabilities',
    id: ActivityId.TreeOfProbabilities,
    type: ActivityType.Nightfall,
    active: false,
    date: new Date('2017-12-05 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.CurseOfOsiris,
    ownership: [Expansions.CurseOfOsiris],
    description: `Tree of Probability requires the Curse of Osiris expansion. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'A Garden World',
    id: ActivityId.AGardenWorld,
    type: ActivityType.Nightfall,
    active: false,
    date: new Date('2017-12-05 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.CurseOfOsiris,
    ownership: [Expansions.CurseOfOsiris],
    description: `A Garden World requires the Curse of Osiris expansion. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Eater of Worlds',
    id: ActivityId.EaterOfWorlds,
    type: ActivityType.Raid,
    active: false,
    date: new Date('2017-12-05 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.CurseOfOsiris,
    ownership: [Expansions.CurseOfOsiris],
    description: ``
  },
  {
    name: 'Escalation Protocol',
    id: ActivityId.SpireOfStars,
    type: ActivityType.ExpansionActivity,
    active: false,
    date: new Date('2018-05-08 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Warmind,
    ownership: [Expansions.Warmind],
    description: ``
  },
  {
    name: 'Spire of Stars',
    id: ActivityId.SpireOfStars,
    type: ActivityType.Raid,
    active: false,
    date: new Date('2018-05-08 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.Warmind,
    ownership: [Expansions.Warmind],
    description: ``
  },
  {
    name: 'Strange Terrain',
    id: ActivityId.StrangeTerrain,
    type: ActivityType.Nightfall,
    active: false,
    date: new Date('2018-05-08 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Warmind,
    ownership: [Expansions.Warmind],
    description: `Strange Terrain requires the Warmind expansion. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Will of the Thousands',
    id: ActivityId.WillOfTheThousands,
    type: ActivityType.Nightfall,
    active: false,
    date: new Date('2018-05-08 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Warmind,
    ownership: [Expansions.Warmind],
    description: `Will of the Thousand requires the Warmind expansion. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'The Insight Terminus',
    id: ActivityId.TheInsightTerminus,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2018-05-08 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Warmind,
    ownership: [],
    description: `The Insight Terminus is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'The Whisper',
    id: ActivityId.TheWhisper,
    type: ActivityType.ExoticMission,
    active: true,
    date: new Date('2024-04-10 17:00:00 UTC'),
    // date: new Date('2018-07-20 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Warmind,
    ownership: [],
    description: `The Whisper exotic mission is free to play for all players. It gives the Whisper of the Worm exotic sniper rifle.`
  },
  {
    name: 'Gambit',
    id: ActivityId.Gambit,
    type: ActivityType.CoreActivity,
    active: true,
    date: new Date('2018-09-14 17:00:00 UTC'),
    maxPlayers: 4,
    releasedWith: Expansions.Forsaken,
    ownership: [],
    description: `Gambit is free to play for all players.

A quest within Gambit gives the Malfeasance exotic hand cannon.
    
Gambit is linked to the title Dredgen.`
  },
  {
    name: 'Last Wish',
    id: ActivityId.LastWish,
    type: ActivityType.Raid,
    active: true,
    date: new Date('2018-09-14 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.Forsaken,
    ownership: [Expansions.Forsaken],
    description: `The Last Wish raid requires the Forsaken pack. It gives the One Thousand Voices exotic fusion rifle. This is a legacy raid and is on a weekly rotation.
    
When this raid is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Last Wish is linked to the title Rivensbane.`
  },
  {
    name: 'Blind Well',
    id: ActivityId.BlindWell,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2018-09-14 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Forsaken,
    ownership: [],
    description: `Blind Well activities are free to play for all players.`
  },
  {
    name: 'Ascendant Challenges',
    id: ActivityId.AscendantChallenge,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2018-09-14 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Forsaken,
    ownership: [Expansions.Forsaken],
    description: `Ascendant challenges require the Forsaken pack to access, and are on a weekly rotation.`
  },
  {
    name: 'Warden of Nothing',
    id: ActivityId.WardenOfNothing,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2018-09-14 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Forsaken,
    ownership: [Expansions.Forsaken],
    description: `Warden of Nothing requires the Forsaken pack. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Broodhold',
    id: ActivityId.Broodhold,
    type: ActivityType.Nightfall,
    active: false,
    date: new Date('2018-09-14 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Forsaken,
    ownership: [Expansions.Forsaken],
    description: `Broodhold requires the Forsaken pack. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'The Hollowed Lair',
    id: ActivityId.TheHollowedLair,
    type: ActivityType.Nightfall,
    active: false,
    date: new Date('2018-09-14 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Forsaken,
    ownership: [Expansions.Forsaken],
    description: `The Hollowed Lair requires the Forsaken pack. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'The Corrupted',
    id: ActivityId.TheCorrupted,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2018-09-14 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Forsaken,
    ownership: [Expansions.Forsaken],
    description: `The Corrupted requires the Forsaken pack. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Shattered Throne',
    id: ActivityId.ShatteredThrone,
    type: ActivityType.Dungeon,
    active: true,
    date: new Date('2018-09-25 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Forsaken,
    ownership: [Expansions.Forsaken],
    description: `The Shattered Throne dungeon requires the Forsaken pack. It gives the Wish-Ender exotic bow. This is a legacy dungeon and is on a weekly rotation.
    
When this dungeon is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.`
  },
  {
    name: 'Black Armory Forges',
    id: ActivityId.BlackArmoryForge,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2018-12-17 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.AnnualPass,
    ownership: [Expansions.AnnualPass],
    description: ``
  },
  {
    name: 'Scourge of the Past',
    id: ActivityId.ScourgeOfThePast,
    type: ActivityType.Raid,
    active: false,
    date: new Date('2018-12-07 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.AnnualPass,
    ownership: [Expansions.AnnualPass],
    description: ``
  },
  {
    name: 'Gambit Prime',
    id: ActivityId.GambitPrime,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2019-03-05 17:00:00 UTC'),
    maxPlayers: 4,
    releasedWith: Expansions.AnnualPass,
    ownership: [Expansions.AnnualPass],
    description: ``
  },
  {
    name: 'Reckoning',
    id: ActivityId.Reckoning,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2019-03-05 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.AnnualPass,
    ownership: [Expansions.AnnualPass],
    description: ``
  },
  {
    name: 'Invitations of the Nine',
    id: ActivityId.InvitationOfTheNine,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2019-03-15 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.AnnualPass,
    ownership: [Expansions.AnnualPass],
    description: ``
  },
  {
    name: 'Zero Hour',
    id: ActivityId.ZeroHour,
    type: ActivityType.ExoticMission,
    active: true,
    date: new Date('2024-05-14 17:00:00 UTC'),
    // date: new Date('2019-05-08 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Forsaken,
    ownership: [],
    description: `The Zero Hour exotic mission is free to play for all players. It gives the Outbreak Perfected exotic pulse rifle.`
  },
  {
    name: 'Crown of Sorrow',
    id: ActivityId.CrownOfSorrow,
    type: ActivityType.Raid,
    active: false,
    date: new Date('2019-06-17 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.AnnualPass,
    ownership: [Expansions.AnnualPass],
    description: ``
  },
  {
    name: 'Menagerie',
    id: ActivityId.Menagerie,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2019-06-17 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.AnnualPass,
    ownership: [Expansions.AnnualPass],
    description: ``
  },
  {
    name: 'The Other Side',
    id: ActivityId.TheOtherSide,
    type: ActivityType.ExoticMission,
    active: false,
    date: new Date('2019-07-09 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.AnnualPass,
    ownership: [Expansions.AnnualPass],
    description: ``
  },
  {
    name: 'Nightmare Hunts',
    id: ActivityId.NightmareHunt,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2019-10-01 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Shadowkeep,
    ownership: [Expansions.Shadowkeep],
    description: `Nightmare Hunts require the Shadowkeep pack, and are on a weekly rotation.`
  },
  {
    name: 'Altar of Sorrow',
    id: ActivityId.AltarOfSorrow,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2019-10-01 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Shadowkeep,
    ownership: [],
    description: `Altars of Sorrow are free to play for all players, and its bosses are on a weekly rotation.`
  },
  {
    name: 'The Scarlet Keep',
    id: ActivityId.TheScarletKeep,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2019-10-01 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Shadowkeep,
    ownership: [Expansions.Shadowkeep],
    description: `The Scarlet Keep is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'The Festering Core',
    id: ActivityId.TheFesteringCore,
    type: ActivityType.Nightfall,
    active: false,
    date: new Date('2019-10-01 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Shadowkeep,
    ownership: [Expansions.Shadowkeep],
    description: `The Festering Core requires the Shadowkeep pack. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Garden of Salvation',
    id: ActivityId.GardenOfSalvation,
    type: ActivityType.Raid,
    active: true,
    date: new Date('2019-10-05 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.Shadowkeep,
    ownership: [Expansions.Shadowkeep],
    description: `The Garden of Salvation raid requires the Shadowkeep pack. It gives the Divinity exotic trace rifle. This is a legacy raid and is on a weekly rotation.
    
When this raid is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Garden of Salvation is linked to the title Enlightened.`
  },
  {
    name: 'Vex Offensive',
    id: ActivityId.VexOffensive,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2019-10-05 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: ShadowkeepSeasonHashes.Undying,
    ownership: [ShadowkeepSeasonHashes.Undying],
    description: ``
  },
  {
    name: 'Pit of Heresy',
    id: ActivityId.PitOfHeresy,
    type: ActivityType.Dungeon,
    active: true,
    date: new Date('2019-10-29 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Shadowkeep,
    ownership: [Expansions.Shadowkeep],
    description: `The Pit of Heresy dungeon requires the Shadowkeep pack. It gives the Xenophage exotic machine gun. This is a legacy dungeon and is on a weekly rotation.
    
When this dungeon is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.`
  },
  {
    name: 'Sundial',
    id: ActivityId.Sundial,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2019-12-10 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: ShadowkeepSeasonHashes.Dawn,
    ownership: [ShadowkeepSeasonHashes.Dawn],
    description: ``
  },
  {
    name: 'Seraph Towers',
    id: ActivityId.SeraphTower,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2020-03-10 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: ShadowkeepSeasonHashes.Worthy,
    ownership: [ShadowkeepSeasonHashes.Worthy],
    description: ``
  },
  {
    name: 'Seraph Bunkers',
    id: ActivityId.SeraphBunker,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2020-03-10 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: ShadowkeepSeasonHashes.Worthy,
    ownership: [ShadowkeepSeasonHashes.Worthy],
    description: ``
  },
  {
    name: 'Trials of Osiris',
    id: ActivityId.TrialsOfOsiris,
    type: ActivityType.CoreActivity,
    active: true,
    // date: new Date('2020-03-13 17:00:00 UTC'), // Real release date, but follows the expansion it's linked to
    date: new Date('2024-06-04 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: ShadowkeepSeasonHashes.Worthy,
    ownership: [Expansions.FinalShape],
    description: `Trials of Osiris requires the latest released expansion and the completion of the Trials Access quest to play.
    
Trials of Osiris is linked to the title Flawless.`
  },
  {
    name: 'Contact',
    id: ActivityId.Contact,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2020-06-09 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: ShadowkeepSeasonHashes.Arrivals,
    ownership: [ShadowkeepSeasonHashes.Arrivals],
    description: ``
  },
  {
    name: 'Interference',
    id: ActivityId.Interference,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2020-06-09 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: ShadowkeepSeasonHashes.Arrivals,
    ownership: [ShadowkeepSeasonHashes.Arrivals],
    description: ``
  },
  {
    name: 'Prophecy',
    id: ActivityId.Prophecy,
    type: ActivityType.Dungeon,
    active: true,
    date: new Date('2020-06-09 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: ShadowkeepSeasonHashes.Arrivals,
    ownership: [],
    description: `The Prophecy dungeon is free to play for all players. This is a legacy dungeon and is on a weekly rotation.
    
When this dungeon is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.`
  },
  {
    name: 'Empire Hunts',
    id: ActivityId.EmpireHunt,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2020-11-10 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.BeyondLight,
    ownership: [Expansions.BeyondLight],
    description: `Empire Hunts require the Beyond Light pack, and are on a weekly rotation.`
  },
  {
    name: 'Exo Challenges',
    id: ActivityId.ExoChallenge,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2020-11-10 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.BeyondLight,
    ownership: [Expansions.BeyondLight],
    description: `Exo Challenges require the Beyond Light pack, and are on a weekly rotation.`
  },
  {
    name: 'The Glassway',
    id: ActivityId.TheGlassway,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2020-11-10 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.BeyondLight,
    ownership: [],
    description: `The Glassway is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'The Disgraced',
    id: ActivityId.TheDisgraced,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2020-11-10 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.BeyondLight,
    ownership: [],
    description: `The Disgraced is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'The Devil\'s Lair',
    id: ActivityId.TheDevilsLair,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2020-11-10 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.BeyondLight,
    ownership: [],
    description: `The Devil's Lair is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Fallen S.A.B.E.R.',
    id: ActivityId.FallenSaber,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2020-11-10 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.BeyondLight,
    ownership: [],
    description: `Fallen S.A.B.E.R. is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Proving Grounds',
    id: ActivityId.ProvingGrounds,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2020-11-10 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.BeyondLight,
    ownership: [],
    description: `Proving Grounds is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Wrathborn Hunts',
    id: ActivityId.WrathbornHunt,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2020-11-17 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: BeyondLightSeasonHashes.Hunt,
    ownership: [BeyondLightSeasonHashes.Hunt],
    description: ``
  },
  {
    name: 'Deep Stone Crypt',
    id: ActivityId.DeepStoneCrypt,
    type: ActivityType.Raid,
    active: true,
    date: new Date('2020-11-21 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.BeyondLight,
    ownership: [Expansions.BeyondLight],
    description: `The Deep Stone Crypt raid requires the Beyond Light pack. It gives the Eyes of Tomorrow exotic rocket launcher. This is a legacy raid and is on a weekly rotation.
    
When this raid is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Deep Stone Crypt is linked to the title Descendant.`
  },
  {
    name: 'Harbinger',
    id: ActivityId.Harbinger,
    type: ActivityType.ExoticMission,
    active: false,
    date: new Date('2020-11-17 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: BeyondLightSeasonHashes.Hunt,
    ownership: [BeyondLightSeasonHashes.Hunt],
    description: ``
  },
  {
    name: 'Battlegrounds',
    id: ActivityId.Battleground,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2021-02-09 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: BeyondLightSeasonHashes.Chosen,
    ownership: [BeyondLightSeasonHashes.Chosen],
    description: ``
  },
  {
    name: 'Presage',
    id: ActivityId.Presage,
    type: ActivityType.ExoticMission,
    active: true,
    date: new Date('2021-02-16 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: BeyondLightSeasonHashes.Chosen,
    ownership: [BeyondLightSeasonHashes.Chosen, Expansions.BeyondLight],
    description: `The Presage exotic mission requires the Beyond Light pack or the Season of the Chosen, and is on a weekly rotation. It gives the Dead Man's Tale exotic scout rifle.
    
When this exotic mission is in rotation, the second completion on each character will award pinnacle gear, and every completion will award at-level gear.`
  },
  {
    name: 'Overrides',
    id: ActivityId.Override,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2021-05-11 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: BeyondLightSeasonHashes.Splicer,
    ownership: [BeyondLightSeasonHashes.Splicer],
    description: ``
  },
  {
    name: 'Vault of Glass',
    id: ActivityId.VaultOfGlass,
    type: ActivityType.Raid,
    active: true,
    date: new Date('2021-05-22 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: BeyondLightSeasonHashes.Splicer,
    ownership: [],
    description: `The Vault of Glass raid is free to play for all players. It gives the Vex Mythoclast exotic fusion rifle. This is a legacy raid and is on a weekly rotation.
    
When this raid is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Vault of Glass is linked to the title Fatebreaker.`
  },
  {
    name: 'Expunges',
    id: ActivityId.Expunge,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2021-05-25 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: BeyondLightSeasonHashes.Splicer,
    ownership: [BeyondLightSeasonHashes.Splicer],
    description: ``
  },
  {
    name: 'Shattered Realms',
    id: ActivityId.ShatteredRealm,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2021-08-24 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: BeyondLightSeasonHashes.Lost,
    ownership: [BeyondLightSeasonHashes.Lost],
    description: ``
  },
  {
    name: 'Dares of Eternity',
    id: ActivityId.DaresOfEternity,
    type: ActivityType.CoreActivity,
    active: true,
    date: new Date('2021-12-07 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.Anniversary30th,
    ownership: [],
    description: `Dares of Eternity are free to play for all players.`
  },
  {
    name: 'Expert Dares of Eternity',
    id: ActivityId.LegendDaresOfEternity,
    type: ActivityType.CoreActivity,
    active: true,
    date: new Date('2021-12-07 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.Anniversary30th,
    ownership: [Expansions.Anniversary30th],
    description: `Expert Dares of Eternity require the 30th Anniversary pack.`
  },
  {
    name: 'Grasp of Avarice',
    id: ActivityId.GraspOfAvarice,
    type: ActivityType.Dungeon,
    active: true,
    date: new Date('2021-12-07 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Anniversary30th,
    ownership: [Expansions.Anniversary30th],
    description: `The Grasp of Avarice dungeon requires the 30th Anniversary pack. It gives the Gjallarhorn exotic rocket launcher. This is a legacy dungeon and is on a weekly rotation.
    
When this dungeon is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.`
  },
  {
    name: 'Wellsprings',
    id: ActivityId.Wellspring,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.TheWitchQueen,
    ownership: [Expansions.TheWitchQueen],
    description: `Wellsprings require the Witch Queen expansion, and are on a weekly rotation.`
  },
  {
    name: 'PsiOps Battlegrounds',
    id: ActivityId.PsiOpsBattleground,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Risen,
    ownership: [WitchQueenSeasonHashes.Risen],
    description: ``
  },
  {
    name: 'The Lightblade',
    id: ActivityId.TheLightblade,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.TheWitchQueen,
    ownership: [Expansions.TheWitchQueen],
    description: `The Lightblade requires the Witch Queen expansion. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Birthplace of the Vile',
    id: ActivityId.BirthplaceOfTheVile,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.TheWitchQueen,
    ownership: [Expansions.TheWitchQueen],
    description: `Birthplace of the Vile requires the Witch Queen expansion. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Battleground: Behemoth',
    id: ActivityId.BattlegroundBehemoth,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: BeyondLightSeasonHashes.Chosen,
    ownership: [],
    description: `Battleground: Behemoth is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Battleground: Hailstone',
    id: ActivityId.BattlegroundHailstone,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: BeyondLightSeasonHashes.Chosen,
    ownership: [],
    description: `Battleground: Hailstone is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Battleground: Foothold',
    id: ActivityId.BattlegroundFoothold,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: BeyondLightSeasonHashes.Chosen,
    ownership: [],
    description: `Battleground: Foothold is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Battleground: Oracle',
    id: ActivityId.BattlegroundOracle,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: BeyondLightSeasonHashes.Chosen,
    ownership: [],
    description: `Battleground: Oracle is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Vox Obscura',
    id: ActivityId.VoxObscura,
    type: ActivityType.ExoticMission,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Risen,
    ownership: [WitchQueenSeasonHashes.Risen, Expansions.TheWitchQueen],
    description: `The Vox Obscura exotic mission requires the Witch Queen expansion or the Season of the Risen, and is on a weekly rotation. It gives the Dead Messenger exotic grenade launcher.
    
When this exotic mission is in rotation, the second completion on each character will award pinnacle gear, and every completion will award at-level gear.`
  },
  {
    name: 'Vow of the Disciple',
    id: ActivityId.VowOfTheDisciple,
    type: ActivityType.Raid,
    active: true,
    date: new Date('2022-03-05 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.TheWitchQueen,
    ownership: [Expansions.TheWitchQueen],
    description: `The Vow of the Disciple raid requires the Witch Queen expansion. It gives the Collective Obligation exotic pulse rifle. This is a legacy raid and is on a weekly rotation.
    
When this raid is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Vow of the Disciple is linked to the title Disciple-Slayer.`
  },
  {
    name: 'Preservation',
    id: ActivityId.Preservation,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2022-03-05 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.TheWitchQueen,
    ownership: [Expansions.TheWitchQueen],
    description: `Preservation requires the Witch Queen expansion.`
  },
  {
    name: 'Nightmare Containments',
    id: ActivityId.NightmareContainment,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2022-05-24 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: WitchQueenSeasonHashes.Haunted,
    ownership: [WitchQueenSeasonHashes.Haunted],
    description: ``
  },
  {
    name: 'Sever',
    id: ActivityId.Sever,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2022-05-24 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Haunted,
    ownership: [WitchQueenSeasonHashes.Haunted],
    description: ``
  },
  {
    name: 'Duality',
    id: ActivityId.Duality,
    type: ActivityType.Dungeon,
    active: true,
    date: new Date('2022-05-27 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Haunted,
    ownership: [WitchQueenDungeonHashes.Duality],
    description: `The Duality dungeon requires the Witch Queen Dungeon Key. It gives the Heartshadow exotic sword. This is a legacy dungeon and is on a weekly rotation.
    
When this dungeon is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Duality is linked to the title Discerptor.`
  },
  {
    name: 'Ketchcrash',
    id: ActivityId.Ketchcrash,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2022-08-23 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: WitchQueenSeasonHashes.Plunder,
    ownership: [WitchQueenSeasonHashes.Plunder],
    description: ``
  },
  {
    name: 'Expeditions',
    id: ActivityId.Expedition,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2022-08-23 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Plunder,
    ownership: [WitchQueenSeasonHashes.Plunder],
    description: ``
  },
  {
    name: 'Pirate Hideouts',
    id: ActivityId.PirateHideout,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2022-08-23 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Plunder,
    ownership: [WitchQueenSeasonHashes.Plunder],
    description: ``
  },
  {
    name: 'King\'s Fall',
    id: ActivityId.KingsFall,
    type: ActivityType.Raid,
    active: true,
    date: new Date('2022-08-26 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: WitchQueenSeasonHashes.Plunder,
    ownership: [],
    description: `The King's Fall raid is free to play for all players. It gives the Touch of Malice exotic scout rifle. This is a legacy raid and is on a weekly rotation.
    
When this raid is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

King's Fall is linked to the title Kingslayer.`
  },
  {
    name: 'Heist Battlegrounds',
    id: ActivityId.HeistBattleground,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2022-12-06 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Seraph,
    ownership: [WitchQueenSeasonHashes.Seraph],
    description: ``
  },
  {
    name: 'Spire of the Watcher',
    id: ActivityId.SpireOfTheWatcher,
    type: ActivityType.Dungeon,
    active: true,
    date: new Date('2022-12-09 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Seraph,
    ownership: [WitchQueenDungeonHashes.SpireOfTheWatcher],
    description: `The Spire of the Watcher dungeon requires the Witch Queen Dungeon Key. It gives the Hierarchy of Needs exotic bow. This is a legacy dungeon and is on a weekly rotation.
    
When this dungeon is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Spire of the Watcher is linked to the title WANTED.`
  },
  {
    name: 'Operation: Seraph Shield',
    id: ActivityId.OperationSeraphShield,
    type: ActivityType.ExoticMission,
    active: true,
    date: new Date('2022-12-20 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Seraph,
    ownership: [WitchQueenSeasonHashes.Seraph, Expansions.TheWitchQueen],
    description: `The Operation: Seraph Shield exotic mission requires the Witch Queen expansion or the Season of the Seraph, and is on a weekly rotation. It gives the Revision Zero exotic pulse rifle.
    
When this exotic mission is in rotation, the second completion on each character will award pinnacle gear, and every completion will award at-level gear.`
  },
  {
    name: 'Terminal Overloads',
    id: ActivityId.TerminalOverload,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2023-02-28 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Lightfall,
    ownership: [Expansions.Lightfall],
    description: `Terminal Overloads require the Lightfall expansion to get rewards.`
  },
  {
    name: 'Partitions',
    id: ActivityId.Partition,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2023-02-28 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Lightfall,
    ownership: [Expansions.Lightfall],
    description: `Partitions require the Lightfall expansion, and are on a weekly rotation.`
  },
  {
    name: 'HyperNet Current',
    id: ActivityId.HyperNetCurrent,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2023-02-28 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Lightfall,
    ownership: [Expansions.Lightfall],
    description: `HyperNet Current requires the Lightfall expansion. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'PsiOps Battleground: EDZ',
    id: ActivityId.PsiOpsBattlegroundEDZ,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Risen,
    ownership: [],
    description: `PsiOps Battleground: EDZ is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'PsiOps Battleground: Cosmodrome',
    id: ActivityId.PsiOpsBattlegroundCosmodrome,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Risen,
    ownership: [],
    description: `PsiOps Battleground: Cosmodrome is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'PsiOps Battleground: Moon',
    id: ActivityId.PsiOpsBattlegroundMoon,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Risen,
    ownership: [],
    description: `PsiOps Battleground: Moon is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Heist Battleground: Europa',
    id: ActivityId.HeistBattlegroundEuropa,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Seraph,
    ownership: [],
    description: `Heist Battleground: Europa is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Heist Battleground: Moon',
    id: ActivityId.HeistBattlegroundMoon,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Seraph,
    ownership: [],
    description: `Heist Battleground: Moon is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Heist Battleground: Mars',
    id: ActivityId.HeistBattlegroundMars,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2022-02-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: WitchQueenSeasonHashes.Seraph,
    ownership: [],
    description: `Heist Battleground: Mars is free to play for all players. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Defiant Battlegrounds',
    id: ActivityId.DefiantBattleground,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2023-02-28 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Defiance,
    ownership: [LightfallSeasonHashes.Defiance],
    description: `Defiant Battlegrounds require the Season of Defiance.`
  },
  {
    name: '//node.ovrd.AVALON//',
    id: ActivityId.NodeOvrdAvalon,
    type: ActivityType.ExoticMission,
    active: true,
    date: new Date('2023-03-07 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Defiance,
    ownership: [LightfallSeasonHashes.Defiance, Expansions.Lightfall],
    description: `The //node.ovrd.AVALON// exotic mission requires the Lightfall expansion or the Season of Defiance, and is on a weekly rotation. It gives the Vexcalibur exotic glaive.`

// When this exotic mission is in rotation, the second completion on each character will award pinnacle gear, and every completion will award at-level gear.`
  },
  {
    name: 'Root of Nightmares',
    id: ActivityId.RootOfNightmares,
    type: ActivityType.Raid,
    active: true,
    date: new Date('2023-03-10 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.Lightfall,
    ownership: [Expansions.Lightfall],
    description: `The Root of Nightmares raid requires the Lightfall expansion. It gives the Conditional Finality exotic shotgun. This is a legacy raid and is on a weekly rotation.
    
When this raid is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Root of Nightmares is linked to the title Dream Warrior.`
  },
  {
    name: 'Deep Dives',
    id: ActivityId.DeepDive,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2023-05-23 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Deep,
    ownership: [LightfallSeasonHashes.Deep],
    description: `Deep Dives require the Season of the Deep.`
  },
  {
    name: 'Salvage',
    id: ActivityId.Salvage,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2023-05-23 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: LightfallSeasonHashes.Deep,
    ownership: [LightfallSeasonHashes.Deep],
    description: `Salvage missions require the Season of the Deep.`
  },
  {
    name: 'Fishing',
    id: ActivityId.Fishing,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2023-05-23 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Deep,
    ownership: [LightfallSeasonHashes.Deep],
    description: `Fishing requires the Season of the Deep.`
  },
  {
    name: 'Ghosts of the Deep',
    id: ActivityId.GhostsOfTheDeep,
    type: ActivityType.Dungeon,
    active: true,
    date: new Date('2023-05-26 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Deep,
    ownership: [LightfallDungeonHashes.GhostsOfTheDeep],
    description: `The Ghosts of the Deep dungeon requires the Lightfall Dungeon Key. It gives the Navigator exotic trace rifle. This is a legacy dungeon and is on a weekly rotation.
    
When this dungeon is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Ghosts of the Deep is linked to the title Ghoul.`
  },
  {
    name: 'Savathûn\'s Spire',
    id: ActivityId.SavathunsSpire,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2023-08-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Witch,
    ownership: [LightfallSeasonHashes.Witch],
    description: `Savathûn's Spire requires the Season of the Witch.`
  },
  {
    name: 'Altars of Summoning',
    id: ActivityId.AltarOfSummoning,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2023-08-22 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Witch,
    ownership: [LightfallSeasonHashes.Witch],
    description: `Altars of Summoning require the Season of the Witch.`
  },
  {
    name: 'Crota\'s End',
    id: ActivityId.CrotasEnd,
    type: ActivityType.Raid,
    active: true,
    date: new Date('2023-09-01 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: LightfallSeasonHashes.Witch,
    ownership: [],
    description: `The Crota's End raid is free to play for all players. It gives the Necrochasm exotic auto rifle. This is a legacy raid and is on a weekly rotation.
    
When this raid is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Crota's End is linked to the title Swordbearer.`
  },
  {
    name: 'Riven\'s Lair',
    id: ActivityId.RivensLair,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2023-11-28 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Wish,
    ownership: [LightfallSeasonHashes.Wish],
    description: `Riven's Lair requires the Season of the Wish.`
  },
  {
    name: 'The Coil',
    id: ActivityId.TheCoil,
    type: ActivityType.SeasonalActivity,
    active: false,
    date: new Date('2023-11-28 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Wish,
    ownership: [LightfallSeasonHashes.Wish],
    description: `The Coil requires the Season of the Wish.`
  },
  {
    name: 'Warlord\'s Ruin',
    id: ActivityId.WarlordsRuin,
    type: ActivityType.Dungeon,
    active: true,
    date: new Date('2023-12-01 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Wish,
    ownership: [LightfallDungeonHashes.WarlordsRuin],
    description: `The Warlord's Ruin dungeon requires the Lightfall Dungeon Key. It gives the Buried Bloodline exotic sidearm. This is a legacy dungeon and is on a weekly rotation.
    
When this dungeon is in rotation, the first completion on each character will award pinnacle gear, and every encounter completion will award at-level gear.

Warlord's Ruin is linked to the title Wrathbearer.`
  },
  {
    name: 'Starcrossed',
    id: ActivityId.Starcrossed,
    type: ActivityType.ExoticMission,
    active: true,
    date: new Date('2023-12-19 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: LightfallSeasonHashes.Wish,
    ownership: [LightfallSeasonHashes.Wish, Expansions.Lightfall],
    description: `The Starcrossed exotic mission requires the Lightfall expansion or the Season of the Wish, and is on a weekly rotation. It gives the Wish-Keeper exotic bow.`

// When this exotic mission is in rotation, the second completion on each character will award pinnacle gear, and every completion will award at-level gear.`
  },
  {
    name: 'Onslaught',
    id: ActivityId.Onslaught,
    type: ActivityType.CoreActivity,
    active: true,
    date: new Date('2024-04-10 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.Lightfall,
    ownership: [],
    description: `Onslaught is free to play for all players.`
  },
  {
    name: 'Overthrow',
    id: ActivityId.Overthrow,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2024-06-04 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.FinalShape,
    ownership: [Expansions.FinalShape],
    description: `Overthrows require The Final Shape expansion.`
  },
  {
    name: 'Liminality',
    id: ActivityId.Liminality,
    type: ActivityType.Nightfall,
    active: true,
    date: new Date('2024-06-04 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: Expansions.FinalShape,
    ownership: [Expansions.FinalShape],
    description: `Liminality requires The Final Shape expansion. Nightfall Strikes grant exclusive weapons and rotate each week. The available Nightfall Strike may require ownership of a certain expansion.
    
Nightfall Strikes are linked to the title Conqueror.`
  },
  {
    name: 'Salvation\'s Edge',
    id: ActivityId.SalvationsEdge,
    type: ActivityType.Raid,
    active: true,
    date: new Date('2024-06-07 17:00:00 UTC'),
    maxPlayers: 6,
    releasedWith: Expansions.FinalShape,
    ownership: [Expansions.FinalShape],
    description: `The Salvation's Edge raid requires The Final Shape expansion. It gives the Euphony exotic linear fusion rifle.
    
This raid will always give pinnacle gear on every encounter for the first completion on each character.

Salvation's Edge is linked to the title Iconoclast.`
  },
  {
    name: 'Excision',
    id: ActivityId.Excision,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2024-06-08 17:00:00 UTC'),
    maxPlayers: 12,
    releasedWith: Expansions.FinalShape,
    ownership: [Expansions.FinalShape],
    description: `Excision requires The Final Shape expansion.`
  },
  {
    name: 'Dual Destiny',
    id: ActivityId.DualDestiny,
    type: ActivityType.ExpansionActivity,
    active: true,
    date: new Date('2024-06-11 17:00:00 UTC'),
    maxPlayers: 2,
    releasedWith: Expansions.FinalShape,
    ownership: [Expansions.FinalShape],
    description: `Dual Destiny requires The Final Shape expansion.`
  },
  {
    name: 'Breach Executable',
    id: ActivityId.BreachExecutable,
    type: ActivityType.SeasonalActivity,
    active: true,
    date: new Date('2024-06-11 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Echoes,
    ownership: [FinalShapeEpisodeHashes.Echoes],
    description: `Breach Executable requires the Episode: Echoes.`
  },
  {
    name: 'Enigma Protocol',
    id: ActivityId.EnigmaProtocol,
    type: ActivityType.SeasonalActivity,
    active: true,
    date: new Date('2024-06-11 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Echoes,
    ownership: [FinalShapeEpisodeHashes.Echoes],
    description: `Enigma Protocol requires the Episode: Echoes.`
  },
  {
    name: 'Echoes Battlegrounds',
    id: ActivityId.EchoesBattlegrounds,
    type: ActivityType.SeasonalActivity,
    active: true,
    date: new Date('2024-07-16 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Echoes,
    ownership: [FinalShapeEpisodeHashes.Echoes],
    description: `Echoes Battlegrounds require the Episode: Echoes.`
  },
  {
    name: 'Encore',
    id: ActivityId.Encore,
    type: ActivityType.ExoticMission,
    active: true,
    date: new Date('2024-08-27 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Echoes,
    ownership: [FinalShapeEpisodeHashes.Echoes],
    description: `The Encore exotic mission requires Episode: Echoes. It gives the Choir of One exotic auto rifle.`
  },
  {
    name: 'Onslaught: Salvation',
    id: ActivityId.OnslaughtSalvation,
    type: ActivityType.SeasonalActivity,
    active: true,
    date: new Date('2024-10-08 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Revenant,
    ownership: [FinalShapeEpisodeHashes.Revenant],
    description: `Onslaught: Salvation requires the Episode: Revenant.`
  },
  {
    name: 'Vesper\'s Host',
    id: ActivityId.VespersHost,
    type: ActivityType.Dungeon,
    active: true,
    date: new Date('2024-10-11 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Revenant,
    ownership: [FinalShapeDungeonHashes.VespersHost],
    description: `The Vesper's Host dungeon requires the Final Shape Dungeon Key. It gives the Ice Breaker exotic sniper rifle.
    
This dungeon will always give pinnacle gear on every encounter for the first completion on each character.

Vesper's Host is linked to the title Unleashed.`
  },
  {
    name: 'Tomb of Elders',
    id: ActivityId.TombOfElders,
    type: ActivityType.SeasonalActivity,
    active: true,
    date: new Date('2024-11-19 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Revenant,
    ownership: [FinalShapeEpisodeHashes.Revenant],
    description: `Tomb of Elders requires the Episode: Revenant.`
  },
  {
    name: 'Kell\'s Fall',
    id: ActivityId.KellsFall,
    type: ActivityType.ExoticMission,
    active: true,
    date: new Date('2025-01-07 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Revenant,
    ownership: [FinalShapeEpisodeHashes.Revenant],
    description: `Kell's Fall requires the Episode: Revenant. It gives the Slayer's Fang exotic shotgun.`
  },
  {
    name: 'The Nether',
    id: ActivityId.TheNether,
    type: ActivityType.SeasonalActivity,
    active: true,
    date: new Date('2025-02-04 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Heresy,
    ownership: [FinalShapeEpisodeHashes.Heresy],
    description: `The Nether requires the Episode: Heresy.`
  },
  {
    name: 'Sundered Doctrine',
    id: ActivityId.SunderedDoctrine,
    type: ActivityType.Dungeon,
    active: true,
    date: new Date('2025-02-07 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Heresy,
    ownership: [FinalShapeDungeonHashes.SunderedDoctrine],
    description: `The Sundered Doctrine dungeon requires the Final Shape Dungeon Key. It gives the Finality's Auger exotic linear fusion rifle.
    
This dungeon will always give pinnacle gear on every encounter for the first completion on each character.

Sundered Doctrine is linked to the title Delver.`
  },
  {
    name: 'Derealize',
    id: ActivityId.Derealize,
    type: ActivityType.ExoticMission,
    active: true,
    date: new Date('2025-02-11 17:00:00 UTC'),
    maxPlayers: 3,
    releasedWith: FinalShapeEpisodeHashes.Heresy,
    ownership: [FinalShapeEpisodeHashes.Heresy],
    description: `Derealize requires the Episode: Heresy. It gives the Barrow-Dyad exotic submachine gun.`
  },
]

const releaseDetailArray: Release[] = [
  {
    name: 'Destiny 2',
    id: Expansions.Destiny2,
    date: new Date('2017-09-06 09:00:00 UTC'),
    bought: 'Free to play',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Curse of Osiris',
    id: Expansions.CurseOfOsiris,
    date: new Date('2017-12-05 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Warmind',
    id: Expansions.Warmind,
    date: new Date('2018-05-08 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Forsaken pack',
    id: Expansions.Forsaken,
    date: new Date('2018-09-17 17:00:00 UTC'),
    bought: 'Costs money',
    active: true,
    hidden: false,
    shortDescription:
`The Forsaken pack gives access to:
  - Forsaken exotic weapons and armor
  - The Last Wish raid
  - The Shattered Throne dungeon`,
    longDescription: [
`The pack gives access to everything beyond the Dreaming City destination, including the raid, dungeon and quests.

> **Note**: The Forsaken campaign and the Tangled Shore location have been removed from the game and are no longer available to play. Dreaming City activities are still available. A Timeline activity is available to catch-up on the story. If you own the Forsaken expansion, you automatically own the Forsaken pack.

## Exotic weapons
The Last Wish raid and the Shattered Throne dungeon give the following exotic weapons:`,
[
  2069224589, // One Thousand Voices (raid)
  814876684 // Wish-Ender (dungeon)
],
`If you have never bought the Forsaken expansion, buying the Forsaken Pack for the first time will give you 3 Forsaken Exotic Ciphers. These can be used to instantly unlock any three non-raid Forsaken exotics at the Monument to Lost Light in the Tower:`,
[
  347366834, // Ace of Spades
  1364093401, // The Last Word (S5)
  3588934839, // Le Monarque (S5)
  417164956, // Jötunn (S5)
  3211806999, // Izanagi's Burden (S5)
  3973202132, // Thorn (S6)
  3512014804, // Lumina (S7)
  1201830623, // Truth (S7)
  2816212794 // Bad Juju (S7)
],
`## Titles
Finally, buying the Forsaken pack makes the Cursebreaker and Rivensbane titles available to unlock.`,
    ]
  },
  {
    name: 'Forsaken Annual Pass',
    id: Expansions.AnnualPass,
    date: new Date('2018-12-04 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Shadowkeep pack',
    id: Expansions.Shadowkeep,
    date: new Date('2019-10-01 17:00:00 UTC'),
    bought: 'Costs money',
    active: true,
    hidden: false,
    shortDescription:
`The Shadowkeep pack gives access to:
  - Shadowkeep exotic weapons and armor
  - The Garden of Salvation raid
  - The Pit of Heresy dungeon
  - Nightmare Hunt activities`,
    longDescription: [
`The pack gives access to everything beyond the Moon destination, including the raid, dungeon, quests and Nightmare Hunts.

> **Note**: The Shadowkeep campaign is now free for all players.

## Campaign rewards
The Shadowkeep campaign gives access to one unique exotic armor piece per class:`,
[
  1537074069, // Phoenix Cradle (Titan)
  2203146422, // Assassin's Cowl (Hunter)
  1996008488 // Stormdancer's Brace (Warlock)
],
`## Exotic weapons
Afterwards, the Pit of Heresy dungeon, the Garden of Salvation raid and an exotic quest give the following exotic weapons:`,
[
  4103414242, // Divinity
  1395261499, // Xenophage
  2232171099 // Deathbringer
],
`## Exotic armor
Additionally, having this pack adds the following exotic armor unlocks to Master Rahool Focused Decoding's third page (after your first reset):`,
[
  2326396534, // Severance Enclosure (Titan, S9)
  1190497097, // Citan's Ramparts (Titan, S10)

  1219761634, // The Bombardiers (Hunter, S9)
  2268523867, // Raiju's Harness (Hunter, S10)

  235591051, // Promethium Spur (Warlock, S9)
  2822465023 // Felwinter's Helm (Warlock, S10)
],
`## Titles
Finally, buying the Shadowkeep pack makes the Harbinger and Enlightened titles available to unlock.`,
    ]
  },
  {
    name: 'Season of Undying',
    id: ShadowkeepSeasonHashes.Undying,
    date: new Date('2019-10-01 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Season of Dawn',
    id: ShadowkeepSeasonHashes.Dawn,
    date: new Date('2019-12-10 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Season of the Worthy',
    id: ShadowkeepSeasonHashes.Worthy,
    date: new Date('2020-03-10 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Season of Arrivals',
    id: ShadowkeepSeasonHashes.Arrivals,
    date: new Date('2020-06-09 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Beyond Light pack',
    id: Expansions.BeyondLight,
    date: new Date('2020-11-10 17:00:00 UTC'),
    bought: 'Costs money',
    active: true,
    hidden: false,
    shortDescription:
`The Beyond Light pack gives access to:
  - Beyond Light exotic weapons and armor
  - The Deep Stone Crypt raid
  - Empire Hunt and Exo Challenge activities
  - The Presage exotic mission
  - Old Season of the Haunted legendary weapons and armor`,
    longDescription: [
`The pack gives access to everything beyond the Europa destination, including the raid, quests, Empire Hunts and Exo Challenges. It also gives access to the Presage exotic mission and old Season of the Haunted legendary weapons and armor.

> **Note**: You get access to the Presage exotic mission if you own either Beyond Light or Season of the Chosen.

> **Note**: The Beyond Light campaign and the Stasis subclass are now free for all players. A Timeline activity is also available to catch-up on the story.

## Exotic weapons
Afterwards, the Deep Stone Crypt raid and exotic quests give the following exotic weapons:`,
[
  2399110176, // Eyes of Tomorrow
  1594120904, // No Time to Explain
  370712896, // Salvation's Grip
  3487253372 // The Lament
],
`Empire Hunts activities have a chance to drop the following exotic weapon:`,
[
  2603483885 // Cloudstrike
],
`The exotic mission gives the following exotic weapon:`,
[
  3654674561 // Dead Man's Tale (S13)
],
`## Exotic armor
Additionally, having this pack adds the following exotic armor unlocks to Master Rahool Focused Decoding's third page (after your first reset):`,
[
  3974038291, // Precious Scars (Titan)
  1467044898, // Icefall Mantle (Titan)
  2321120637, // Cuirass of the Falling Star (Titan, S13)
  1453120846, // The Path of Burning Steps (Titan, S14)
  3267996858, // No Backup Plans (Titan, S15)

  1619425569, // Mask of Bakris (Hunter)
  2415768376, // Athrys's Embrace (Hunter)
  1935198785, // Omnioculus (Hunter, S13)
  1001356380, // Star-Eater Scales (Hunter, S14)
  1702288800, // Radiant Dance Machines (Hunter, S15)

  2316914168, // Dawn Chorus (Warlock)
  2780717641, // Necrotic Grips (Warlock)
  3301944824, // Mantle of Battle Harmony (Warlock, S13)
  3045642045, // Boots of the Assembler (Warlock, S14)
  300502917 // Nothing Manacles (Warlock, S15)
],
`## Titles
Finally, buying the Beyond Light pack makes the Splintered and Descendant titles available to unlock.`,
    ]
  },
  {
    name: 'Season of the Hunt',
    id: BeyondLightSeasonHashes.Hunt,
    date: new Date('2020-11-10 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Season of the Chosen',
    id: BeyondLightSeasonHashes.Chosen,
    date: new Date('2021-02-09 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: true,
    hidden: false,
    shortDescription: 
`Season of the Chosen gives access to:
  - The Presage exotic mission
  - Old Season of the Haunted legendary weapons and armor`,
    longDescription: [
`The season gives access to old Season of the Haunted seasonal armor and weapons and the Presage exotic mission.

> **Note**: You get access to the Presage exotic mission if you own either Beyond Light or Season of the Chosen.

## Exotic weapons
The exotic mission gives the following exotic weapon:`,
[
  3654674561 // Dead Man's Tale
],
    ]
  },
  {
    name: 'Season of the Splicer',
    id: BeyondLightSeasonHashes.Splicer,
    date: new Date('2021-05-11 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Season of the Lost',
    id: BeyondLightSeasonHashes.Lost,
    date: new Date('2021-08-24 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: '30th Anniversary pack',
    id: Expansions.Anniversary30th,
    date: new Date('2021-12-07 17:00:00 UTC'),
    bought: 'Costs money',
    active: true,
    hidden: false,
    shortDescription:
`The 30th Anniversary pack gives access to:
  - The Gjallarhorn exotic rocket launcher
  - Dares of Eternity exclusive armor ornaments
  - The Grasp of Avarice dungeon`,
    longDescription: [
`The pack gives access to the Grasp of Avarice dungeon. The dungeon gives the following exotic weapon:`,
[
  1363886209 // Gjallarhorn
],
`## Other rewards
Additionally, the pack gives access to the Expert Dares of Eternity activity, and other cosmetic rewards in Xûr's Treasure Hoard.

## Titles
Finally, buying the 30th Anniversary pack makes the Forerunner title available to unlock`
    ]
  },
  {
    name: 'The Witch Queen',
    id: Expansions.TheWitchQueen,
    date: new Date('2022-02-22 17:00:00 UTC'),
    bought: 'Costs money',
    active: true,
    hidden: false,
    shortDescription:
`The Witch Queen gives access to:
  - The Witch Queen exotic weapons and armor
  - The Witch Queen campaign
  - The Vow of the Disciple raid
  - Wellspring and Preservation activities
  - The Vox Obscura and Operation: Seraph Shield exotic missions
  - Old Season of the Risen and Season of the Seraph legendary weapons and armor`,
    longDescription: [
`The expansion gives access to everything beyond the Throne World destination, including the full campaign, raid, quests, Wellspring and Preservation. It also gives access to the Vox Obscura and Operation: Seraph Shield exotic missions and old Season of the Risen and Season of the Seraph legendary weapons and armor.

> **Note**: You get access to the Vox Obscura exotic mission if you own either The Witch Queen or Season of the Risen.

> **Note**: You get access to the Operation: Seraph Shield exotic mission if you own either The Witch Queen or Season of the Seraph.

> **Note**: The first campaign mission of The Witch Queen is free for all players. A Timeline activity is also available to catch-up on the story.

## Campaign rewards
Finishing the Witch Queen campaign on Expert difficulty instantly grants one of two exotic armor pieces per class:`,
[
  3316517958, // Loreley Splendor Helm (Titan)
  1322544481, // Hoarfrost-Z (Titan)

  1703551922, // Blight Ranger (Hunter)
  2169905051, // Renewal Grasps (Hunter)

  3259193988, // Osmiomancy Gloves (Warlock)
  511888814 // Secant Filaments (Warlock)
],
`## Exotic weapons
Afterwards, the Vow of the Disciple raid, Wellspring activities and exotic quests give the following exotic weapons:`,
[
  3505113722, // Collective Obligation
  46524085, // Osteo Striga
  542203595, // Edge of Concurrence
  2535142413, // Edge of Action
  14194600, // Edge of Intent
  2812324400 // Parasite
],
`The exotic missions give the following exotic weapons:`,
[
  2812324401, // Dead Messenger
  1473821207 // Revision Zero (S19)
],
`## Exotic armor
Additionally, having this expansion adds the following exotic armor unlocks to Master Rahool Focused Decoding's third page (after your first reset):`,
[
  3316517958, // Loreley Splendor Helm (Titan)
  1322544481, // Hoarfrost-Z (Titan)
  1443166262, // Second Chance (Titan, S17)
  1703598057, // Point-Contact Cannon Brace (Titan, S18)

  1703551922, // Blight Ranger (Hunter)
  2169905051, // Renewal Grasps (Hunter)
  3453042252, // Caliban's Hand (Hunter, S17)
  461841403, // Gyrfalcon's Hauberk (Hunter, S18)

  3259193988, // Osmiomancy Gloves (Warlock)
  511888814, // Secant Filaments (Warlock)
  1624882687, // Rain of Fire (Warlock, S17)
  1849149215 // Fallen Sunstar (Warlock, S18)
],
`## Titles
Finally, buying The Witch Queen makes the Gumshoe and Disciple-Slayer titles available to unlock.`,
    ]
  },
  {
    name: 'The Witch Queen Dungeon Key',
    id: WitchQueenDungeonHashes.Duality,
    date: new Date('2022-05-27 17:00:00 UTC'),
    bought: 'Costs 2000 Silver',
    active: true,
    hidden: false,
    shortDescription:
`The Witch Queen Dungeon Key gives access to:
  - The Duality dungeon
  - The Spire of the Watcher dungeon`,
    longDescription: [
`The dungeon key gives access to the Duality and Spire of the Watcher dungeons. Those give the following exotic weapons:`,
[
  3664831848, // Heartshadow (S17)
  4174431791 // Hierarchy of Needs (S19)
],
`## Titles
Finally, buying The Witch Queen Dungeon Key makes the Discerptor and WANTED titles available to unlock.`,
    ]
  },
  {
    name: 'The Witch Queen Dungeon Key',
    id: WitchQueenDungeonHashes.SpireOfTheWatcher,
    date: new Date('2022-12-09 17:00:00 UTC'),
    bought: 'Costs 2000 Silver',
    active: false,
    hidden: true,
    shortDescription:
`The Witch Queen Dungeon Key gives access to:
  - The Duality dungeon
  - The Spire of the Watcher dungeon`,
    longDescription: [
`The dungeon key gives access to the Duality and Spire of the Watcher dungeons. Those give the following exotic weapons:`,
[
  3664831848, // Heartshadow (S17)
  4174431791 // Hierarchy of Needs (S19)
],
`## Titles
Finally, buying The Witch Queen Dungeon Key makes the Discerptor and WANTED titles available to unlock.`,
    ]
  },
  {
    name: 'Season of the Risen',
    id: WitchQueenSeasonHashes.Risen,
    date: new Date('2022-02-22 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: true,
    hidden: false,
    shortDescription: 
`Season of the Risen gives access to:
  - The Vox Obscura exotic mission
  - Old Season of the Risen legendary weapons and armor`,
    longDescription: [
`The season gives access to old Season of the Risen seasonal armor and weapons and the Vox Obscura exotic mission.

> **Note**: You get access to the Vox Obscura exotic mission if you own either The Witch Queen or Season of the Risen.

## Exotic weapons
The exotic mission gives the following exotic weapon:`,
[
  2812324401 // Dead Messenger
],
    ]
  },
  {
    name: 'Season of the Haunted',
    id: WitchQueenSeasonHashes.Haunted,
    date: new Date('2022-05-24 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Season of Plunder',
    id: WitchQueenSeasonHashes.Plunder,
    date: new Date('2022-08-23 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Season of the Seraph',
    id: WitchQueenSeasonHashes.Seraph,
    date: new Date('2022-12-06 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: true,
    hidden: false,
    shortDescription: 
`Season of the Seraph gives access to:
  - The Operation: Seraph Shield exotic mission
  - Old Season of the Seraph legendary weapons and armor`,
    longDescription: [
`The season gives access to old Season of the Seraph seasonal armor and weapons and the Operation: Seraph Shield exotic mission.

> **Note**: You get access to the Operation: Seraph Shield exotic mission if you own either The Witch Queen or Season of the Seraph.

## Exotic weapons
The exotic mission gives the following exotic weapon:`,
[
  1473821207 // Revision Zero (S19)
],
    ]
  },
  {
    name: 'Lightfall',
    id: Expansions.Lightfall,
    date: new Date('2023-02-28 17:00:00 UTC'),
    bought: 'Costs money',
    active: true,
    hidden: false,
    shortDescription:
`Lightfall gives access to:
  - Lightfall exotic weapons and armor
  - The Lightfall campaign
  - The Strand subclass
  - The Root of Nightmares raid
  - Terminal Overload and Partition activities`,
    longDescription: [
`The expansion gives access to everything beyond the Neomuna destination, including the full campaign, raid, quests, Terminal Overload rewards and Partitions. It also gives access to the //node.ovrd.AVALON// and Starcrossed exotic missions and old Season of Defiance and Season of the Wish legendary weapons and armor.

> **Note**: You get access to the //node.ovrd.AVALON// exotic mission if you own either Lightfall or Season of Defiance.

> **Note**: You get access to the Starcrossed exotic mission if you own either Lightfall or Season of the Wish.

> **Note**: The first campaign mission of Lightfall is free for all players. A Timeline activity is also available to catch-up on the story.

## Campaign rewards
The Lightfall campaign gives access to the Strand subclass and a lot of aspects and fragments.

> ***Warning***: Contrary to most loot, which are unlocks independant of content ownership, switching to a platform where you do not own the Lightfall expansion will prevent equipping and using the Strand subclass. Play on a platform where you own this content to use it.

Finishing the Lightfall campaign on Expert difficulty instantly grants one of two exotic armor pieces per class:`,
[
  3574051505, // Cadmus Ridge Lancecap (Titan)
  3637722482, // Abeyant Leap (Titan)

  192896783, // Cyrtarachne's Facade (Hunter)
  2390471904, // Speedloader Slacks (Hunter)

  3831935023, // Ballidorse Wrathweavers (Warlock)
  2463947681 // Swarmers (Warlock)
],
`## Exotic weapons
Afterwards, the Root of Nightmares raid and exotic quests give the following exotic weapons:`,
[
  3371017761, // Conditional Finality
  3121540812, // Final Warning
  449318888, // Deterministic Chaos
  3118061004 // Winterbite
],
`## Exotic armor
Additionally, having this expansion adds the following exotic armor unlocks to Master Rahool Focused Decoding's third page (after your first reset):`,
[
  3574051505, // Cadmus Ridge Lancecap (Titan)
  3637722482, // Abeyant Leap (Titan)
  90009855, // Arbor Warden (Titan, S21)
  2066430310, // Pyrogale Gauntlets (Titan, S22)

  192896783, // Cyrtarachne's Facade (Hunter)
  2390471904, // Speedloader Slacks (Hunter)
  3093309525, // Triton Vice (Hunter, S21)
  3534173884, // Mothkeeper's Wraps (Hunter, S23)

  3831935023, // Ballidorse Wrathweavers (Warlock)
  2463947681, // Swarmers (Warlock)
  2374129871, // Cenotaph Mask (Warlock, S21)
  3234692237 // Briarbinds (Warlock, S22)
],
`## Titles
Finally, buying the Lightfall expansion makes the Virtual Fighter and Dream Warrior titles available to unlock.`,
    ],
    deluxeDescription: 
`The deluxe edition includes:
  - The Lightfall Dungeon key
  - Season of Defiance, Season of the Deep, Season of the Witch, and Season of the Wish`
  },
  {
    name: 'Lightfall Dungeon Key',
    id: LightfallDungeonHashes.GhostsOfTheDeep,
    date: new Date('2023-05-26 17:00:00 UTC'),
    bought: 'Costs 2000 Silver',
    active: true,
    hidden: false,
    shortDescription:
`The Lightfall Dungeon Key gives access to:
  - The Ghosts of The Deep dungeon
  - The Warlord's Ruin dungeon`,
    longDescription: [
`The dungeon key gives access to the Ghosts of The Deep and Warlord's Ruin dungeons. Those give the following exotic weapons:`,
[
  1441805468, // The Navigator (S21)
  3886719505 // Buried Bloodline (S23)
],
`## Titles
Finally, buying The Lightfall Dungeon Key makes the Ghoul and Wrathbearer titles available to unlock.`,
    ]
  },
  {
    name: 'Lightfall Dungeon Key',
    id: LightfallDungeonHashes.WarlordsRuin,
    date: new Date('2023-12-01 17:00:00 UTC'),
    bought: 'Costs 2000 Silver',
    active: false,
    hidden: true,
    shortDescription:
`The Lightfall Dungeon Key gives access to:
  - The Ghosts of The Deep dungeon
  - The Warlord's Ruin dungeon`,
    longDescription: [
`The dungeon key gives access to the Ghosts of The Deep and Warlord's Ruin dungeons. Those give the following exotic weapons:`,
[
  1441805468, // The Navigator (S21)
  3886719505 // Buried Bloodline (S23)
],
`## Titles
Finally, buying The Lightfall Dungeon Key makes the Ghoul and Wrathbearer titles available to unlock.`,
    ]
  },
  {
    name: 'Season of Defiance',
    id: LightfallSeasonHashes.Defiance,
    date: new Date('2023-02-28 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: true,
    hidden: false,
    shortDescription: 
`Season of Defiance gives access to:
  - The //node.ovrd.AVALON// exotic mission
  - Old Season of Defiance legendary weapons and armor`,
    longDescription: [
`The season gives access to old Season of Defiance seasonal armor and weapons and the //node.ovrd.AVALON// exotic mission.

> **Note**: You get access to the //node.ovrd.AVALON// exotic mission if you own either Lightfall or Season of Defiance.

## Exotic weapons
The exotic mission gives the following exotic weapon:`,
[
  3118061005 // Vexcalibur
],
    ]
  },
  {
    name: 'Season of the Deep',
    id: LightfallSeasonHashes.Deep,
    date: new Date('2023-05-23 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Season of the Witch',
    id: LightfallSeasonHashes.Witch,
    date: new Date('2023-08-22 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: false,
    hidden: false,
    shortDescription: '',
    longDescription: []
  },
  {
    name: 'Season of the Wish',
    id: LightfallSeasonHashes.Wish,
    date: new Date('2023-11-28 17:00:00 UTC'),
    bought: 'Not purchaseable',
    active: true,
    hidden: false,
    shortDescription: 
`Season of the Wish gives access to:
  - The Starcrossed exotic mission
  - Old Season of the Wish legendary weapons and armor`,
    longDescription: [
`The season gives access to old Season of the Wish seasonal armor and weapons and the Starcrossed exotic mission.

> **Note**: You get access to the Starcrossed exotic mission if you own either Lightfall or Season of the Wish.

## Exotic weapons
The exotic mission gives the following exotic weapon:`,
[
  2910326942 // Wish-Keeper (S23)
],
    ]
  },
  {
    name: 'The Final Shape',
    id: Expansions.FinalShape,
    date: new Date('2024-06-04 17:00:00 UTC'),
    bought: 'Costs money',
    active: true,
    hidden: false,
    shortDescription:
`Final Shape gives access to:
  - Final Shape exotic weapons and armor
  - The Final Shape campaign
  - The full Prismatic subclass
  - The Salvation's Edge raid
  - Overthrow and Excision activities`,
    longDescription: [
`The expansion gives access to everything beyond the Pale Heart destination, including the full campaign, raid, quests and Overthrow, Excision and Dual Destiny activities.

## Campaign rewards
The Final Shape campaign gives access to the full Prismatic subclass and a lot of aspects and fragments.

> **Note**: The first campaign mission of The Final Shape is free for all players. Prismatic is unlocked through the first mission, but additional abilities, aspects and fragments require the full campaign and quests to acquire.

Finishing The Final Shape campaign on Expert difficulty instantly grants one of two exotic armor pieces per class:`,
[
  77153391, // Hazardous Propulsion (Titan)
  3717431477, // Wishful Ignorance (Titan)

  1627691271, // Gifted Conviction (Hunter)
  3001449507, // Balance of Power (Hunter)

  50291571, // Speaker's Sight (Warlock)
  1955548646 // Mataiodoxía (Warlock)
],
`## Exotic weapons
Afterwards, the Salvation's Edge raid and exotic quests give the following exotic weapons and armor:`,
[
  3284383335, // Euphony
  1681583613, // Ergo Sum
  4129629253, // Khvostov 7G-0X
  4207066264, // Microcosm
  2905188646, // Still Hunt
  266021826,  // Stoicism
  2273643087, // Solipsism
  2809120022  // Relativism
],
`## Exotic armor
Additionally, having this expansion adds the following exotic armor unlocks to Master Rahool Focused Decoding's third page (after your first reset):`,
[
  77153391, // Hazardous Propulsion (Titan)
  3717431477, // Wishful Ignorance (Titan)

  1627691271, // Gifted Conviction (Hunter)
  3001449507, // Balance of Power (Hunter)

  50291571, // Speaker's Sight (Warlock)
  1955548646 // Mataiodoxía (Warlock)
],
`## Titles
Finally, buying The Final Shape expansion makes the Transcendent and Iconoclast titles available to unlock.`,
    ],
    deluxeDescription: 
`The deluxe edition includes:
  - The Final Shape Dungeon key
  - Episode: Echoes, Episode: Revenant and Episode: Heresy`
  },
  {
    name: 'The Final Shape Dungeon Key',
    id: FinalShapeDungeonHashes.VespersHost,
    date: new Date('2024-10-11 17:00:00 UTC'),
    bought: 'Costs 2000 Silver',
    active: true,
    hidden: false,
    shortDescription:
`The Final Shape Dungeon Key gives access to:
  - The Vesper's Host dungeon
  - The Sundered Doctrine dungeon`,
    longDescription: [
`The dungeon key gives access to the Vesper's Host and the Sundered Doctrine dungeons. It give the following exotic weapons:`,
[
  1111334348, // Ice Breaker (S25)
  331231237, // Finality's Auger (S26)
],
`## Titles
Finally, buying The Final Shape Dungeon Key makes the Unleashed and Delver titles available to unlock.`,
    ]
  },
  {
    name: 'The Final Shape Dungeon Key',
    id: FinalShapeDungeonHashes.SunderedDoctrine,
    date: new Date('2024-06-04 17:00:00 UTC'),
    bought: 'Costs 2000 Silver',
    active: false,
    hidden: true,
    shortDescription:
``,
    longDescription: []
  },
  {
    name: 'Episode: Echoes',
    id: FinalShapeEpisodeHashes.Echoes,
    date: new Date('2024-06-11 17:00:00 UTC'),
    bought: 'Costs 1500 Silver',
    active: true,
    hidden: false,
    shortDescription: 
`Episode: Echoes gives access to:
  - The Encore exotic mission
  - Failsafe (H.E.L.M.) legendary weapons and armor
  - Breach Executable, Echoes Battlegrounds and Enigma Protocol activities`,
    longDescription: [
`The episode gives access to Failsafe (H.E.L.M.) upgrades, bounties and seasonal armor and weapons, Breach Executable, Echoes Battlegrounds, Enigma Protocol and the Encore exotic mission.

## Exotic weapons
The exotic mission gives the following exotic weapon:`,
[
  3698448090 // Choir of One (S24)
],
`## Titles
Finally, buying the Episode: Echoes makes the Intrepid title available to unlock.`,
    ]
  },
  {
    name: 'Episode: Revenant',
    id: FinalShapeEpisodeHashes.Revenant,
    date: new Date('2024-10-08 17:00:00 UTC'),
    bought: 'Costs 1500 Silver',
    active: true,
    hidden: false,
    shortDescription: 
`Episode: Revenant gives access to:
  - The Kell's Fall exotic mission
  - Eido (The City) tonics, legendary weapons and armor
  - Onslaught: Salvation, Tomb of Elders activities`,
    longDescription: [
`The episode gives access to Eido (The City) tonics, upgrades, bounties and seasonal armor and weapons, Onslaught: Salvation, Tomb of Elders and the Kell's Fall exotic mission.

## Exotic weapons
The exotic mission gives the following exotic weapon:`,
[
  1047932517 // Slayer's Fang (S25)
],
`## Titles
Finally, buying the Episode: Revenant makes the Slayer Baron title available to unlock.`,
    ]
  },
  {
    name: 'Episode: Heresy',
    id: FinalShapeEpisodeHashes.Heresy,
    date: new Date('2025-02-04 17:00:00 UTC'),
    bought: 'Costs 1500 Silver',
    active: true,
    hidden: false,
    shortDescription: 
`Episode: Heresy gives access to:
  - Shaping Slab (The City) upgrades, legendary weapons and armor
  - The Nether activity
  - The Derealize exotic mission
  - More to come...`,
    longDescription: [
`The episode gives access to Shaping Slab (The City) upgrades, Tome of Want, bounties, seasonal armor and weapons, The Nether, the Derealize exotic mission, and more to come...

## Exotic weapons
The exotic mission gives the following exotic weapon:`,
[
  1481594633 // Barrow-Dyad (S26)
],
`## Titles
Finally, buying the Episode: Heresy makes the Heretic title available to unlock.`,
    ]
  },
]

export const releaseDetail = new Map(releaseDetailArray.map(obj => [ obj.id, obj ]))
