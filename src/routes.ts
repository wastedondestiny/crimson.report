import { createRouter, createWebHistory } from 'vue-router'
import Home from './components/Home.vue'
// import ExpansionsPage from './components/ExpansionsPage.vue'
// import AboutPage from './components/AboutPage.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      name: 'Home',
      path: '/:memberships(.*)*',
      component: Home
    },
    /* {
      name: 'ExpansionsPage',
      path: '/expansions',
      component: ExpansionsPage
    }, */
    /* {
      name: 'AboutPage',
      path: '/about',
      component: AboutPage
    }, */
  ]
})

export default router
